FROM node:8-alpine AS builder

RUN apk update && apk upgrade && \

    apk add --no-cache bash git openssh

WORKDIR /usr/devoxxmareg/frontend
COPY package.json ./
RUN npm install
COPY . ./
RUN npm run build
FROM nginx
EXPOSE 80
COPY --from=builder /usr/devoxxmareg/frontend/build /usr/share/nginx/html
COPY --from=builder /usr/devoxxmareg/frontend/.nginx-config /etc/nginx/conf.d/default.conf
